from fastapi import FastAPI
from starlette.responses import FileResponse


app = FastAPI()


@app.get('/favicon.ico', include_in_schema=False)
async def favicon():
    return FileResponse('favicon.ico')


@app.get("/")
async def root():
    return {"message": "Hello World!!"}
